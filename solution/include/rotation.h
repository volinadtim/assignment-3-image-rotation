#pragma once

#include "image.h"

int16_t rotate(struct image *img, struct image *new_image, int angle);

#pragma once

enum read_status
{
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MALLOC_ERROR,
    READ_FILE_ENDED_UNEXPECTEDLY_ERROR,
    /* коды других ошибок  */
};

enum write_status
{
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

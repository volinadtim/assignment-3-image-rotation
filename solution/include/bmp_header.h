#pragma once

#include <stdint.h>

#define FILE_TYPE 0x4D42
#define RESERVED 0
#define OFF_BITS sizeof(struct bmp_header)
#define COMPRESSION_TYPE 0
#define BIT_COUNT (PIXEL_SIZE * 8)
#define HEADER_SIZE 40
#define PLANES 1
#define X_PELS_PER_METER 1
#define Y_PELS_PER_METER 1
#define CLR_USED 0
#define CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

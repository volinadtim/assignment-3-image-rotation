#pragma once

#define PIXEL_SIZE sizeof(struct pixel)

#include <stdint.h>

struct pixel {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};

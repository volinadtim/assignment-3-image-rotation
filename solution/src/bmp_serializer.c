#include "bmp_serializer.h"
#include "bmp_header.h"
#include "pixel.h"

#include <stdio.h>

uint8_t get_padding(uint8_t width)
{
    return 4 - (width * PIXEL_SIZE) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    printf("bmp_serializer.c 14");
    struct bmp_header bmp_header;

    if (fread(&bmp_header, sizeof(bmp_header), 1, in) != 1)
    {
        fprintf(stderr, "READ_INVALID_HEADER\n");
        return READ_INVALID_HEADER;
    }

    if (bmp_header.bfType != FILE_TYPE)
    {
        fprintf(stderr, "READ_INVALID_SIGNATURE\n");
        return READ_INVALID_SIGNATURE;
    }

    if (bmp_header.biBitCount != BIT_COUNT)
    {
        fprintf(stderr, "READ_INVALID_BITS: now is %d, should be %lu\n", bmp_header.biBitCount, BIT_COUNT);
        return READ_INVALID_BITS;
    }
    printf("bmp_serializer.c 34");

    img->width = bmp_header.biWidth;
    img->height = bmp_header.biHeight;
    img->data = malloc(PIXEL_SIZE * bmp_header.biWidth * bmp_header.biHeight);

    if (!img->data)
    {
        fprintf(stderr, "READ_MALLOC_ERROR\n");
        free(img);
        return READ_MALLOC_ERROR;
    }

    for (int i = 0; i < img->height; i++)
    {
        if (fread(&img->data[i * img->width], PIXEL_SIZE, img->width, in) != img->width)
        {
            free(img->data);
            fprintf(stderr, "READ_FILE_ENDED_UNEXPECTEDLY_ERROR\n");
            return READ_FILE_ENDED_UNEXPECTEDLY_ERROR;
        }
        // Shift cursor for paddings
        fseek(in, get_padding(img->width), SEEK_CUR);
    }
    printf("bmp_serializer.c 57");

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img)
{
    uint32_t sizeImage = ((uint32_t)(img->width * PIXEL_SIZE + get_padding(img->width))) * img->height;

    struct bmp_header header = (struct bmp_header){
        .bfType = FILE_TYPE,
        .bfReserved = RESERVED,
        .bfileSize = sizeImage + OFF_BITS,
        .bOffBits = OFF_BITS,
        .biSize = HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION_TYPE,
        .biSizeImage = sizeImage,
        .biXPelsPerMeter = X_PELS_PER_METER,
        .biYPelsPerMeter = Y_PELS_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT};

    if (fwrite(&header, sizeof(header), 1, out) < 1)
    {
        return WRITE_ERROR;
    }

    uint8_t padding = get_padding(img->width);

    uint8_t padding_array[3] = {0};
    for (uint32_t i = 0; i < img->height; i++)
    {
        void *buff = &img->data[i * img->width];
        if (fwrite(buff, PIXEL_SIZE, img->width, out) != img->width)
        {
            return WRITE_ERROR;
        }

        if (fwrite(padding_array, 1, padding, out) != padding)
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

#include "rotation.h"
#include "pixel.h"

#include <stdio.h>
#include <stdlib.h>

struct image *clone(struct image const *original_image, struct image *new_image)
{
    new_image->width = original_image->width;
    new_image->height = original_image->height;

    for (int i = 0; i < original_image->width; i++)
    {
        for (int j = 0; j < original_image->height; j++)
        {
            new_image->data[original_image->height * i + j] = original_image->data[original_image->height * i + j];
        }
    }

    return new_image;
}

struct image *rotate_90(struct image const *original_image, struct image *new_image)
{
    new_image->width = original_image->height;
    new_image->height = original_image->width;

    for (int i = 0; i < original_image->width; i++)
    {
        for (int j = 0; j < original_image->height; j++)
        {
            new_image->data[original_image->height * i + j] = original_image->data[j * original_image->width + (original_image->width - 1 - i)];
        }
    }

    return new_image;
}

struct image *rotate_180(struct image const *original_image, struct image *new_image)
{
    new_image->width = original_image->width;
    new_image->height = original_image->height;

    for (int i = 0; i < original_image->width; i++)
    {
        for (int j = 0; j < original_image->height; j++)
        {
            new_image->data[original_image->height * i + j] = original_image->data[(original_image->height) * (original_image->width - i - 1) + (original_image->height - j - 1)];
        }
    }

    return new_image;
}

struct image *rotate_270(struct image const *original_image, struct image *new_image)
{
    new_image->width = original_image->height;
    new_image->height = original_image->width;

    for (int i = 0; i < original_image->width; i++)
    {
        for (int j = 0; j < original_image->height; j++)
        {
            new_image->data[original_image->height * i + j] = original_image->data[(original_image->height - j - 1) * original_image->width + i];
        }
    }

    return new_image;
}

int16_t rotate(struct image *img, struct image *new_image, int angle)
{
    int normalized_angle = (angle % 360 + 360) % 360;
    
    switch (normalized_angle)
    {
    case 0:
        clone(img, new_image);
        break;
    case 90:
        rotate_90(img, new_image);
        break;
    case 180:
        rotate_180(img, new_image);
        break;
    case 270:
        rotate_270(img, new_image);
        break;
    default:
        fprintf(stderr, "Incompatible angle");
        return 0;
    }

    return 1;
}

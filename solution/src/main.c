#include "bmp_serializer.h"
#include "enums.h"
#include "fileio.h"
#include "image.h"
#include "pixel.h"
#include "rotation.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
    if (argc == 2 && (strcmp(argv[1], "-h") != 0))
    {
        printf("Usage: ./image-transformer [SOURCE_IMAGE] [TRANSFORMED_IMAGE] [ANGLE]\n");
        return 0;
    }

    if (argc != 4)
    {
        printf("Usage: ./image-transformer [SOURCE_IMAGE] [TRANSFORMED_IMAGE] [ANGLE]\n");
        return -1;
    }

    FILE *input_file = open_file(argv[1], "rb");
    FILE *output_file = open_file(argv[2], "wb");
    if (!input_file || !output_file)
        return -4;

    int angle = atoi(argv[3]);

    struct image img;
    enum read_status bmp_read_status = from_bmp(input_file, &img);

    if (bmp_read_status != READ_OK)
    {
        return -2;
    }

    struct image *rotated_img = malloc(sizeof(struct image));
    rotated_img->data = malloc(sizeof(struct pixel) * img.width * img.height);
    rotate(&img, rotated_img, angle);

    enum write_status bmp_write_status = to_bmp(output_file, rotated_img);

    if (bmp_write_status != WRITE_OK)
    {
        return -3;
    }

    free(img.data);
    free(rotated_img->data);

    close_file(input_file);
    close_file(output_file);

    printf("Successfully wrote image.");

    return 0;
}

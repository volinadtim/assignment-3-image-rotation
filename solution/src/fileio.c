#include "fileio.h"

#include <stdio.h>
#include <stdlib.h>

FILE *open_file(char *filename, char *mode)
{
    FILE *file = fopen(filename, mode);
    if (file == NULL)
    {
        fprintf(stderr, "File \"%s\" not found", filename);
    }
    return file;
}

void close_file(FILE *file)
{
    fclose(file);
}
